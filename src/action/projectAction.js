import axios from 'axios';

var test = false;
var testUrl = test ? "http://10.0.0.147:3000/" : "";
var testEnd = test ? ".json" : "";
var method = test ? "get" : "post";
var pjc = require('./test/pdf_json_eng.json');

export function loadData(sessionID) {
    return new Promise((resolve) => {
        axios({
            method: method,
            url: `${testUrl}eSignature/loadProcessResult${testEnd}`,
            data: {sessionID},
            headers: {
                "Content-Type": "application/json"
            }
        }).then((response) => {
            resolve(response.data);
        }).catch((error) => {
            console.log("error: ", error);
            resolve(false);
        });
    });
}

export function loadImage(data) {
    return new Promise((resolve) => {
        axios({
            method: method,
            url: `${testUrl}eSignature/loadImage${testEnd}`,
            data: data,
            headers: {
                "Content-Type": "application/json"
            }
        }).then((response) => {
            resolve(response.data);
        }).catch((error) => {
            console.log("error: ", error);
            resolve(false);
        });
    });
}

export function saveSignature(data) {
    return new Promise((resolve) => {
        axios({
            method: method,
            url: `${testUrl}eSignature/saveSignature${testEnd}`,
            data: {
                sessionID: data.sessionID,
                signatureFields: data.signatureFields,
                imgStore: data.imgStore,
                PDFlang: data.PDFlang,
                pageHeight: data.pageHeight,
                pageWidth: data.pageWidth
            },
            headers: {
                "Content-Type": "application/json"
            }
        }).then((response) => {
            resolve(response.data);
        }).catch((error) => {
            console.log("error: ", error);
            resolve(false);
        });
    });
}