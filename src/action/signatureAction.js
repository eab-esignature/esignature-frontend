
import { ACTION_TYPES, REDUCER_TYPES } from "../constants";
const { CONTEXT } = REDUCER_TYPES;
var actionObj = require('../action/projectAction');

export const initContext = (context, callback) => {
    callback({
        type: ACTION_TYPES[CONTEXT].INIT_CONTEXT,
        context: context
    });
};

export const loadData = (sessionID, callback) => {
    if (sessionID) {
        actionObj.loadData(sessionID).then((result) => {
            // console.log(JSON.stringify(result));
            if (result && result.Status === 'Success') {
                const context = result;
                initContext(context, callback);
                let newConfig = Object.assign({}, result.config);
                if (result.pageHeight || result.pageWidth) {
                    newConfig.pageWidth = result.pageWidth || null;
                    newConfig.pageHeight = result.pageHeight || null;
                }
                setConfig(newConfig, callback);
                setContext({ complete: true }, callback);
            } else {
                let errorMsg = null;
                if (result.Status === "Fail") {
                    if (result.exception.length > 0) {
                        errorMsg = `${result.exception[0].reasonTXT}: ${result.exception[0].subReasonTXT}`;
                    }
                }
                if (!errorMsg) {
                    errorMsg = "Server Error!";
                }
                alert(errorMsg);
            }
        });
    } else {
        alert("sessionID is underfind");
    }
};

export const loadImage = (data, callback) => {
    actionObj.loadImage(data).then((result) => {
        // console.log(JSON.stringify(result));
        if (result && result.Status === 'Success') {
            callback(result);
        } else {
            let errorMsg = null;
            if (result.Status === "Fail") {
                if (result.exception.length > 0) {
                    errorMsg = `${result.exception[0].reasonTXT}: ${result.exception[0].subReasonTXT}`;
                }
            }
            if (!errorMsg) {
                errorMsg = "Server Error!";
            }
            alert(errorMsg);
        }
    });
};

export const saveSignature = (data, callback) => {
    if (data.sessionID) {
        // console.log(JSON.stringify(data));
        actionObj.saveSignature(data).then((result) => {
            if (result && result.Status === 'Success') {
                console.log("save complete");
                window.parent.postMessage({save: true},'*'); //window.postMessage
            } else {
                let errorMsg = null;
                if (result.Status === "Fail") {
                    if (result.exception.length > 0) {
                        errorMsg = `${result.exception[0].reasonTXT}: ${result.exception[0].subReasonTXT}`;
                    }
                }
                if (!errorMsg) {
                    errorMsg = "Server Error!";
                }
                alert(errorMsg);
            }
        });
    }
}

export const setContext = (context, callback) => {
    callback({
        type: ACTION_TYPES[CONTEXT].SET_CONTEXT,
        context: context
    });
};

export const setConfig = (config, callback) => {
    callback({
        type: ACTION_TYPES[CONTEXT].SET_CONFIG,
        config: config
    });
};
