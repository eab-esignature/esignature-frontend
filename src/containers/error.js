import React, { Component } from 'react';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "@reach/router";
import styles from "../assets/containers/style/error";

class Error extends Component {

  render() {
    const { classes } = this.props;
    return (
      <p className={classes.container}>
        <h1>404</h1>
        <hr />
        <Link className={classes.black} to={"/"}>Home</Link>
      </p>
    );
  }
}

Error.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Error);
