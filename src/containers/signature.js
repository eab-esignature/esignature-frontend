import React, { Component, lazy, Suspense } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, ButtonBase } from "@material-ui/core";
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';

import SignPagination from "../components/signPagination";
import SignSideBar from "../components/signSideBar";
import SignCanvas from "../components/signCanvas";
import MsgDialog from "../components/msgDialog";
import * as _ from "lodash";
import i18n from '../i18n';
import util from "../tool/util";
import styles from "../assets/containers/style/signature";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';

var signatureAction = require('../action/signatureAction');
const SignView = lazy(() => import("../components/signView"));

class Signature extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessionID: null,
      pageIndex: 0,
      pageCount: 0,
      imgStore: [],
      signatureFields: [],
      pdfMainContent: [],
      allDone: false,
      canvasOpen: false,
      sideBarOpen: true,
      sideBarLoad: false,
      selected: null,
      msgOpen: false
    };
  }

  receiveMessageFromIndex = (event) => {
    console.log( '我是iframe,我接受到了：', event.data );
    const sessionID = event.data.sessionID;
    if (sessionID && !_.isEmpty(sessionID)) {
      signatureAction.setContext({
        sessionID,
        pageIndex: 0
      }, (action) => {
        this.props.dispatch(action);
      });
      signatureAction.loadData(sessionID, (action) => {
        if (action.type) {
          this.props.dispatch(action);
        }
      });
      this.initSignature();
    }
  }

  componentDidMount() {
    //监听message事件
    window.addEventListener("message", this.receiveMessageFromIndex, false);
    
    const { sessionID, config } = this.props.context;
    if (config) {
      const { sideBarDefaultOpen } = config;
      if (!_.isEqual(sideBarDefaultOpen, this.state.sideBarOpen)) {
        this.setState({
          sideBarOpen: sideBarDefaultOpen
        });
      }
    }
  }

  componentDidUpdate(prevProps) {
    let newState = {};
    let updateFlag = false;
    const { context } = this.props;
    const { pageIndex, sessionID, pageCount, signatureFields, complete } = context;

    if (!_.isEqual(this.state.pageIndex, pageIndex)) {
      newState.pageIndex = pageIndex;
      updateFlag = true;
    }

    if (context) {
      if (!_.isEqual(this.state.pageCount, pageCount)) {
        newState.pageCount = pageCount;
        newState.pdfMainContent = this.initPdfMainContent(pageCount);
        updateFlag = true;
      }
      if (!_.isEqual(this.state.signatureFields, signatureFields)) {
        newState.signatureFields = signatureFields;
        updateFlag = true;
      }
    }

    if (updateFlag) {
      this.setState(newState, () => {
        this.getSignView(this.state.pageIndex);
      });
    }
  }

  initSignature = () => {
    const { context } = this.props;
    const { pageCount, sessionID, signatureFields } = context;
    const pdfMainContent = [];
    for (let i = 0; i < pageCount; i++) {
      pdfMainContent.push({
        pageIndex: i
      });
    }
    this.setState({
      sessionID: sessionID,
      pageIndex: 0,
      pageCount: pageCount,
      signatureFields: signatureFields,
      pdfMainContent: this.initPdfMainContent(pageCount)
    }, () => {
      this.getSignView(0);
    });
  }

  initPdfMainContent(pageCount) {
    const pdfMainContent = [];
    for (let i = 0; i < pageCount; i++) {
      pdfMainContent.push({
        pageIndex: i
      });
    }
    return pdfMainContent;
  }

  getSignView(index) {
    const { pdfMainContent, signatureFields, imgStore, selected } = this.state;
    let changeFlag = false;
    var curContentIndex = pdfMainContent.findIndex((value) => value.pageIndex === index);
    if (curContentIndex > -1) {
      let curContent = pdfMainContent[curContentIndex];
      let signatureBoxs = _.filter(signatureFields, (item) => {
        return Number(item.pageIndex) === index;
      });
      curContent.container = (
        <Suspense fallback={<div>loading...</div>}>
          <SignView
            imgStore={imgStore}
            pageIndex={curContent.pageIndex}
            selected={selected}
            signatureBoxs={signatureBoxs}
            onClickSignBox={this.handleOpenSignCanvas}
            onClearSignBox={this.handleClearSelectSign}
          />
        </Suspense>
      );
      changeFlag = true;
    }
    if (changeFlag) {
      this.setState({ pdfMainContent });
    }
  }

  checkAllDone = () => {
    const { signatureFields } = this.state;
    const { autoSave } = this.props.context.config;
    let allDone = false;
    if (signatureFields) {
      allDone = signatureFields.findIndex((item) => {
        return !item.imgKey;
      }) === -1;
    }
    const newState = {
      allDone: allDone,
      sideBarLoad: true
    }
    if (allDone && autoSave) {
      newState.msgOpen = true;
    }
    this.setState(newState);
  }

  removeUnusingImgStore = (beforeImgKey) => {
    const { signatureFields, imgStore } = this.state;
    if (beforeImgKey) {
      const checkImgKey = signatureFields.findIndex((item) => {
        return item.imgKey === beforeImgKey;
      });
      if (checkImgKey === -1) {
        const imgIndex = util.findImgStore(imgStore, beforeImgKey);
        imgStore.splice(imgIndex, 1);
        this.setState({
          imgStore: imgStore
        });
      }
    }
  }

  mappingImgStore(signIndex, imgData, canvasRate) {
    const { signatureFields, imgStore } = this.state;
    const beforeImgKey = signatureFields[signIndex].imgKey;
    let imgItem = {
      key: `imgKey${(new Date()).valueOf()}`,
      lookupkey: signatureFields[signIndex].lookupkey,
      height: signatureFields[signIndex].height,
      width: signatureFields[signIndex].width,
      canvasRate: canvasRate,
      data: imgData,
    };
    signatureFields[signIndex].imgKey = imgItem.key;
    signatureFields[signIndex].signDate = new Date();
    imgStore.push(imgItem);
    this.setState({
      imgStore,
      signatureFields,
      selected: null,
      sideBarLoad: false
    }, () => {
      this.getSignView(this.state.pageIndex);
      this.removeUnusingImgStore(beforeImgKey);
      this.checkAllDone();
    });
  }

  usePrevious = (signIndex, imgKey) => {
    const { signatureFields } = this.state;
    const beforeImgKey = signatureFields[signIndex].imgKey;
    if (imgKey) {
      if (!_.isEqual(imgKey, beforeImgKey)) {
        signatureFields[signIndex].imgKey = imgKey;
        signatureFields[signIndex].signDate = new Date();
      }
    }
    this.setState({
      signatureFields,
      selected: null,
      sideBarLoad: false
    }, () => {
      this.getSignView(this.state.pageIndex);
      this.removeUnusingImgStore(beforeImgKey);
      this.checkAllDone();
    });
  }

  handleChangePageIndex = (pageIndex) => {
    signatureAction.setContext({ pageIndex }, (action) => {
      if (action.type) {
        this.props.dispatch(action);
      }
      this.getSignView(pageIndex);
    });
  }

  handleChangePageZoom = (zoom) => {
    signatureAction.setContext({ zoom }, (action) => {
      if (action.type) {
        this.props.dispatch(action);
      }
    });
  }

  handleOpenSignCanvas = (seq, lookupkey) => {
    this.setState({
      selected: {
        seq: seq,
        lookupkey: lookupkey
      },
      canvasOpen: true,
    })
  }

  handleCloseSignature = () => {
    this.setState({
      canvasOpen: false,
      sideBarLoad: false
    }, () => {
      this.getSignView(this.state.pageIndex);
      this.setState({
        sideBarLoad: true
      });
    });
  }

  handleClearSelectSign = (seq, lookupKey) => {
    const { signatureFields } = this.state;
    const signIndex = util.findSignatureFields(signatureFields, seq, lookupKey);
    if (signIndex > -1) {
      const beforeImgKey = signatureFields[signIndex].imgKey;
      signatureFields[signIndex].imgKey = null;
      signatureFields[signIndex].signDate = null;
      this.setState({
        signatureFields,
        sideBarLoad: false
      }, () => {
        this.removeUnusingImgStore(beforeImgKey);
        this.getSignView(this.state.pageIndex);
        this.setState({
          allDone: false,
          sideBarLoad: true
        });
      });
    }
  }

  handleApplyDraw = (imgData, usePrevious = false, canvasRate) => {
    const { selected, signatureFields } = this.state;
    const { seq, lookupkey } = selected;
    const curSignIndex = util.findSignatureFields(signatureFields, seq, lookupkey);
    if (curSignIndex > -1) {
      if (usePrevious) {
        this.usePrevious(curSignIndex, imgData);
      } else {
        this.mappingImgStore(curSignIndex, imgData, canvasRate);
      }
    }
  }

  handleSideBarOpen = () => {
    this.setState({
      sideBarOpen: !this.state.sideBarOpen
    });
  }

  handleSideBarSelected = (selected, pageIndex, openCanvas) => {
    if (_.isEqual(this.state.selected, selected) && !openCanvas) {
      selected = null;
    }
    this.setState({
      selected: selected
    }, () => {
      this.handleChangePageIndex(Number(pageIndex));
      if (openCanvas) {
        this.setState({
          canvasOpen: openCanvas
        });
      }
    });
  }

  handleSaveSignature = () => {
    const { PDFlang, config } = this.props.context;
    const { pageHeight, pageWidth } = config;
    const { sessionID, signatureFields, imgStore } = this.state;
    util.convertImgStore(imgStore).then((newImgStore) => {
      if (sessionID) {
        let data = {
          sessionID: sessionID,
          signatureFields: signatureFields,
          imgStore: newImgStore,
          PDFlang: PDFlang,
          pageHeight,
          pageWidth
        };
        signatureAction.saveSignature(data, (action) => {
          if (action.type) {
            this.props.dispatch(action);
          }
        });
      }
    });
  }

  getMessageDialog() {
    const { localization, config } = this.props.context;
    const { allDone } =this.state;
    const messageDialog = {};
    const { autoSaveMsg } = config;
    if (allDone) {
      messageDialog.title = i18n(localization, { path: "msgDialog.saveTitle" });
      messageDialog.msg = config[`autoSaveMsg${localization}`] ? config[`autoSaveMsg${localization}`] : autoSaveMsg;
      messageDialog.buttons = [
        {
          seq: 0,
          title: i18n(localization, { path: "msgDialog.cannel" }),
          type: "close",
          action: () => {
            this.setState({
              msgOpen: false
            });
          }
        }, {
          seq: 1,
          title: i18n(localization, { path: "msgDialog.save" }),
          type: "submit",
          action: () => {
            if (allDone) {
              this.handleSaveSignature();
            }
            this.setState({
              msgOpen: false
            });
          }
        }
      ]
    }
    return messageDialog;
  }

  render() {
    const { classes, context } = this.props;
    const { config } = context;
    const {
      topBarBGColor,
      pageSpacing,
      pageBoxBGColor,
      signViewMaxZoom,
    } = config;
    let toolBarStyle = { backgroundColor: topBarBGColor };

    const {
      pageIndex,
      pageCount,
      pdfMainContent,
      canvasOpen,
      imgStore,
      signatureFields,
      sideBarOpen,
      sideBarLoad,
      selected,
      allDone,
      msgOpen
    } = this.state;

    let canvasHistory = null;
    let canvasPrevious = null;
    let canvasPreviousKey = null;
    let canvasWidth = null;
    let canvasHeight = null;
    if (selected) {
      const { seq, lookupkey } = selected;
      const curSignIndex = util.findSignatureFields(signatureFields, seq, lookupkey);
      if (curSignIndex > -1) {
        let signatureField = signatureFields[curSignIndex];
        canvasWidth = signatureField.width;
        canvasHeight = signatureField.height;

        let previous = imgStore.filter((item) => {
          return item.lookupkey === lookupkey;
        })
        if (previous.length > 0) {
          canvasPrevious = previous[previous.length - 1].data;
          canvasPreviousKey = previous[previous.length - 1].key;
        }
        if (signatureField.imgKey) {
          let signImgIndex = util.findImgStore(imgStore, signatureField.imgKey);
          canvasHistory = imgStore[signImgIndex].data;
        }
      }
    }
    // const {...other} = this.getMessageDialog();
    const expansPanneSize = "300px";
    return (
      <Grid className={classes.root} container direction="row" style={sideBarOpen ? { paddingRight: expansPanneSize } : null}>
        <Grid className={classes.mainView}>
          <Grid className={classes.topBar} style={toolBarStyle} container direction="row" justify="space-between" alignItems="center">
            <Grid className={classes.topBarItem}>
              <SignPagination pageIndex={pageIndex} pageCount={pageCount} setPageIndex={(nextIndex) => {
                this.handleChangePageIndex(nextIndex);
              }} />
            </Grid>
            <Grid className={classes.topBarItem}>
              <Grid container direction="row" justify="space-between" alignItems="center">
                <Grid className={classes.topToolMenu} container direction="row" justify="center" alignItems="center">
                  <Grid>
                    <ButtonBase className={classes.topToolBtn}
                      disabled={signViewMaxZoom && context.zoom + 0.1 > signViewMaxZoom}
                      onClick={() => { this.handleChangePageZoom(context.zoom + 0.1) }}
                    >
                      <ZoomInIcon />
                    </ButtonBase>
                  </Grid>
                  <Grid>
                    <ButtonBase className={classes.topToolBtn} disabled={context.zoom === 1} onClick={() => { this.handleChangePageZoom(context.zoom - 0.1) }}>
                      <ZoomOutIcon />
                    </ButtonBase>
                  </Grid>
                  <Grid>
                    <ButtonBase className={classes.topToolBtn} onClick={this.handleSideBarOpen}>
                      <DoubleArrowIcon style={sideBarOpen ? {} : { transform: "rotate(180deg)" }} />
                    </ButtonBase>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid container direction="column" alignItems="center"
            className={classes.container} style={{ paddingBottom: pageSpacing, backgroundColor: pageBoxBGColor }}
          >
            {pdfMainContent ? pdfMainContent.map((item, inx) => {
              if (pageIndex === item.pageIndex) {
                if (item.container) {
                  return (
                    <Grid key={inx}>
                      {item.container}
                    </Grid>
                  );
                } else {
                  return <div key={inx}></div>;
                }
              } else {
                return (
                  <Grid key={inx} style={{ display: "none" }}>
                    {item.container}
                  </Grid>
                );
              }
            }) : null}
          </Grid>
        </Grid>
        <Grid style={{ width: expansPanneSize }}>
          <SignSideBar
            open={sideBarOpen}
            fields={signatureFields}
            loading={sideBarLoad}
            selected={selected}
            onSelected={this.handleSideBarSelected}
            onSave={allDone ? this.checkAllDone : null}
          />
        </Grid>
        <SignCanvas
          open={canvasOpen}
          history={canvasHistory}
          previous={canvasPrevious}
          previousKey={canvasPreviousKey}
          canvasWidth={canvasWidth}
          canvasHeight={canvasHeight}
          onSave={this.handleApplyDraw}
          onClose={this.handleCloseSignature}
        />
        <MsgDialog open={msgOpen} {...this.getMessageDialog()}/>
      </Grid>
    );
  }
}

Signature.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.any
};
export default connect(mapStateToProps)(withStyles(styles)(Signature));
