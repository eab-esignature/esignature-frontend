import React, { Component, cloneElement } from "react";
import PropTypes from "prop-types";
import util from "../tool/util";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';
var signatureAction = require('../action/signatureAction');

class Main extends Component {

  componentDidMount() {
    util.geolocation((initialPosition) => {
      signatureAction.setContext({
        gps: {
          //经度
          longitude: initialPosition.coords.longitude,
          //纬度
          latitude: initialPosition.coords.latitude
        }
      }, (action) => {
        this.props.dispatch(action);
      });
    });
  }

  render() {
    const { children } = this.props;
    return <section>{cloneElement(children, { key: "main" })}</section>;
  }
}

Main.propTypes = {
  children: PropTypes.any
};

export default connect(mapStateToProps)(Main);
