import * as _ from "lodash";

const geolocation = (callback) => {
    navigator.geolocation.getCurrentPosition(
        (initialPosition) => {
            callback(initialPosition);
        },
        (error) => {
          console.log(error.message);
        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
}

const getQueryString = (path, name) => {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = path.match(reg); //获取url中"?"符后的字符串并正则匹配
    // var r = window.location.search.substr(1).match(reg); //获取url中"?"符后的字符串并正则匹配
    var context = null;
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return !context ? null : context;
}

const findImgStore = (imgStore, key) => {
    if (key) {
        return imgStore.findIndex((item) => {
            return item.key === key;
        });
    }
    return -1;
}

const findSignatureFields = (signatureFields, seq, lookupkey) => {
    if (seq && lookupkey) {
        return signatureFields.findIndex((item) => {
            return item.seq === seq && item.lookupkey === lookupkey;
        });
    }
    return -1;
}

const calcMagnification = (smallNum, maxSmallNum, bigNum, maxBigNum) => {
    let multiply = maxBigNum / bigNum;
    let big = maxBigNum;
    let small = smallNum * multiply;
    if (small > maxSmallNum) {
        multiply = maxSmallNum / small;
        big = big * multiply;
        small = maxSmallNum;
    }
    return {
        small,
        big
    }
}

const convertImgToBase64 = (data, canvasRate, outputFormat) => {
    return new Promise((resolve) => {
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function () {
            var width = img.width;
            var height = img.height;
            // 按canvasRate 压缩
            var rate = canvasRate;
            canvas.width = width * rate;
            canvas.height = height * rate;
            ctx.drawImage(img, 0, 0, width, height, 0, 0, width * rate, height * rate);
            var dataURL = canvas.toDataURL(outputFormat || 'image/png');
            resolve(Object.assign({}, data, { data: dataURL }));
            canvas = null;
        };
        img.src = data.data;
    });
}

const convertImgStore = (imgStore) => {
    return new Promise((resolve, reject) => {
        let promises = [];
        _.forEach(imgStore, (item) => {
            promises.push(convertImgToBase64(item, item.canvasRate));
        });
        return Promise.all(promises).then((newImgStore) => {
            resolve(newImgStore);
        }).catch((error) => {
            console.error("Error in convertImgStore->Promise.all: ", error);
            reject(error);
        });
    });
}

export default {
    geolocation,
    getQueryString,
    findImgStore,
    findSignatureFields,
    calcMagnification,
    convertImgStore,
    convertImgToBase64
};