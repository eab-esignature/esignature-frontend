import { ACTION_TYPES, REDUCER_TYPES } from "../constants";
import config from "../config/config";
import * as _ from "lodash";
const { CONTEXT } = REDUCER_TYPES;
var test = false;

var getInitState = function() {
    return {
        config: config,
        localization: "EN",
        sessionID: test ? "sda" : null,
        docId: null,
        pageIndex: 0,
        pageCount: 0,
        signatureFields: [],
        signatureRules: [],
        PDFlang: "EN",
        zoom: 1,
        gps: null,
        complete: true
    };
};

export default function signatureInfo(state = getInitState(), action) {
  switch (action.type) {
    case ACTION_TYPES[CONTEXT].INIT_CONTEXT:
        let newContext = {};
        for (var key in action.context) {
            if (key !== "config") {
                newContext[key] = action.context[key];
            }
        }
        return Object.assign({}, state, newContext);
    case ACTION_TYPES[CONTEXT].SET_CONTEXT:
        return Object.assign({}, state, action.context);
    case ACTION_TYPES[CONTEXT].SET_CONFIG:
        state.config = Object.assign({}, state.config, action.config);
        return state;
    case ACTION_TYPES[CONTEXT].INITIAL_APP:
    case ACTION_TYPES[CONTEXT].LOGOUT:
        return getInitState();  // reset the state
    default:
        return state;
  }
};