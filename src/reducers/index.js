// import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from "redux";

import context from "./context";

const rootReducer = combineReducers({
  // Add reducer here
  context
});

export default rootReducer;