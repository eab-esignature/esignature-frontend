import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, ButtonBase, Dialog } from "@material-ui/core";
import styles from "../assets/components/style/msgDialog";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';

class MsgDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidUpdate(prevProps) {
        
    }
    componentDidMount() {
        
    }

    getButtons = () => {
        const { classes, context, buttons } = this.props;
        if (buttons) {
            const {
                applyBtnColor,
                applyBtnBGColor,
                applyBtnDisabledColor,
                applyBtnDisabledBGColor,
            } = context.config;
            return buttons.map((item) => {
                let btnStyle = {};
                if (item.type === "submit") {
                    btnStyle.color = applyBtnColor;
                    btnStyle.backgroundColor = applyBtnBGColor;
                }
                if (item.disabled) {
                    btnStyle.color = applyBtnDisabledColor;
                    btnStyle.backgroundColor = applyBtnDisabledBGColor;
                }
                return (
                    <ButtonBase key={item.seq} onClick={item.action}
                        style={btnStyle} disabled={item.disabled}
                        className={item.disabled ? classes.msgButtonDisable : classes.msgButton}
                    >
                        {item.title}
                    </ButtonBase>
                );
            });
        }
        return null;
    }

    render() {
        const { classes, open, msg, title } = this.props;
        return (
            <Dialog open={open} maxWidth={false}>
                <Grid className={classes.root}>
                    <Grid className={classes.msgTitle}>
                        {title}
                    </Grid>
                    <Grid className={classes.msgSpacing}></Grid>
                    <Grid className={classes.msgContent}>
                        {msg}
                    </Grid>
                    <Grid className={classes.msgSpacing}></Grid>
                    <Grid className={classes.dialogAction} container direction="row" justify="flex-end" alignItems="center">
                        {this.getButtons()}
                    </Grid>
                </Grid>
            </Dialog>
        );
    }
}

MsgDialog.propTypes = {
    classes: PropTypes.object.isRequired
};
export default connect(mapStateToProps)(withStyles(styles)(MsgDialog));
