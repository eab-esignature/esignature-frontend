import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, ButtonBase } from "@material-ui/core";
import ClearIcon from '@material-ui/icons/Clear';
import RefreshIcon from '@material-ui/icons/Refresh';
import moment from 'moment'; 
import * as _ from "lodash";
import i18n from "../i18n";
import util from "../tool/util";
import styles from "../assets/components/style/signView";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';
// var util = require('../tool/util');
var signatureAction = require('../action/signatureAction');

class SignView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imgData: "",
            signatureBoxs: [],
            hover: null
        };
    }

    componentDidMount() {
        const { signatureBoxs } = this.props;
        const { imgData } = this.state;
        if (!imgData) {
            this.loadImageData();
        } else {
            this.setState({ signatureBoxs: signatureBoxs });
        }
    }

    componentDidUpdate(prevProps) {
        let newState = {};
        let updateFlag = false;
        const { signatureBoxs } = this.props;

        const { imgData } = this.state;
        if (!imgData) {
            this.loadImageData();
        }

        if (!_.isEqual(signatureBoxs, prevProps.signatureBoxs)) {
            newState.signatureBoxs = signatureBoxs;
            updateFlag = true;
        }

        if (updateFlag) {
            this.setState(newState);
        }
    }

    loadImageData = () => {
        const { pageIndex, signatureBoxs } = this.props;
        const { context } = this.props;
        let data = {
            sessionID: context.sessionID,
            pageIndex: `${pageIndex + 1}`
        }
        signatureAction.loadImage(data, (result) => {
            if (result) {
                this.setState({
                    imgData: result.data,
                    signatureBoxs: signatureBoxs
                });
            }
        });
    }

    handleHover = (seq, lookupkey) => {
        if (seq && lookupkey) {
            this.setState({hover: { seq, lookupkey }});
        } else {
            this.setState({hover: null});
        }
    }

    render() {
        const { classes, context, onClickSignBox, onClearSignBox, imgStore, selected } = this.props;
        const { localization, config, zoom, gps }  = context;
        const {
            pageHeight,
            pageWidth,
            pageSpacing,
            showSignBox,
            signBoxColor,
            signBoxFontSize,
            signBoxBGColor,
            signBoxHoverBGColor,
            signImgBGColor,
            signImgBorderSize,
            signImgHoverBorderColor,
            signImgSelectedBorderColor,
            signImgClear,
            onlySelectedSignImgClear,
            signLabelColor,
            signLabelFontSize,
            showSignDate,
            signDateFormat,
            signDateMoveX,
            signDateMoveY,
            showGPS,
            gpsDesimalPoint,
            gpsFailPath,
            gpsFormat,
        } = config;
        const { imgData, signatureBoxs, hover } = this.state;
        let pageStyle = {
            marginTop: pageSpacing,
            transform: `scale(${zoom || 1})`
        };

        let pageImgStyle = {};
        if (pageHeight) {
            pageImgStyle.height = pageHeight;
        }
        if (pageWidth) {
            pageImgStyle.width = pageWidth;
        }
        let signLabelStyle = {
            color: signLabelColor,
            fontSize: signLabelFontSize,
            width: "100%",
            textAlign: "center"
        };
        let signDateStyle = Object.assign(signLabelStyle, {
            marginLeft: signDateMoveX ? `${signDateMoveX}px` : null,
            marginTop: signDateMoveX ? `${signDateMoveY}px` : null
        });

        let gpsText = gpsFailPath;
        if (gps) {
            let longitude = gps.longitude.toFixed(gpsDesimalPoint);
            let latitude = gps.latitude.toFixed(gpsDesimalPoint);
            gpsText = gpsFormat.replace(/{x}/, longitude).replace(/{y}/, latitude);
        }
        return imgData ? (
            <Grid className={classes.root} style={pageStyle}>
                {
                    !showSignBox ? null : 
                    signatureBoxs.map((item, inx) => {
                        const signatureBoxStyle = {
                            width: `${item.width}pt`,
                            height: `${item.height}pt`,
                            color: signBoxColor,
                            fontSize: signBoxFontSize,
                            backgroundColor: signBoxBGColor
                        };
                        const clearBtnHidden = {};
                        if (item.imgKey) {
                            if (!hover || (onlySelectedSignImgClear && hover.seq !== item.seq)) {
                                clearBtnHidden.display = "none";
                            }
                        } else {
                            clearBtnHidden.display = "none";
                        }
                        
                        if (selected && item.seq === selected.seq && item.lookupkey === selected.lookupkey) {
                            signatureBoxStyle.border = `${signImgSelectedBorderColor} ${signImgBorderSize} solid`;
                        } else if (hover && hover.seq === item.seq && hover.lookupkey === item.lookupkey) {
                            signatureBoxStyle.backgroundColor = signBoxHoverBGColor;
                            if (item.imgKey) {
                                signatureBoxStyle.border = `${signImgHoverBorderColor} ${signImgBorderSize} solid`;
                            }
                        }

                        const signatureImgStyle = Object.assign(signatureBoxStyle,  {
                            backgroundImage: item.imgKey ? `url(${imgStore[util.findImgStore(imgStore, item.imgKey)].data})` : null,
                            backgroundColor: signImgBGColor
                        });
                        
                        const itemStyle = item.imgKey ? signatureImgStyle : signatureBoxStyle;

                        const signatureItem = {
                            position: "absolute",
                            width: `${item.width}pt`,
                            height: `${item.height}pt`,
                            bottom: `calc(${item.y}pt - ${signatureBoxStyle.height})`,
                            left: `calc(${item.x}pt - ${signatureBoxStyle.width})`
                        };
                        return (
                            <Grid container direction="row" alignItems="center"
                                style={signatureItem} key={inx}
                                onMouseOver={() => {this.handleHover(item.seq, item.lookupkey)}}
                                onMouseOut={() => {this.handleHover(null)}}
                            >
                                <Grid>
                                    <Grid container direction="row" justify="center" alignItems="center"
                                        className={item.imgKey ? classes.signatureImg : classes.signatureBox}
                                        style={itemStyle}
                                        onClick={() => { onClickSignBox(item.seq, item.lookupkey) }}
                                    >
                                        {item.imgKey ? null : <Grid>{i18n(localization, { path: "signView.signatureBox.clickTip" })}</Grid>}
                                    </Grid>
                                </Grid>
                                {!signImgClear ? null :
                                    <ButtonBase
                                        style={clearBtnHidden}
                                        className={classes.clearBtn}
                                        onClick={() => { onClearSignBox(item.seq, item.lookupkey) }}
                                    >
                                        <ClearIcon />
                                    </ButtonBase>
                                }
                                {!item.signDate || !showSignDate ? null :
                                    <Grid style={signDateStyle}>{moment(item.signDate).format(signDateFormat)}</Grid>
                                }
                                {!item.signDate || !showGPS ? null :
                                    <Grid style={signLabelStyle}>{gpsText}</Grid>
                                }
                            </Grid>
                        );
                    })
                }
                <img className={classes.pageImg} style={pageImgStyle} src={`data:image/jpeg;base64,${imgData}`} alt={''} />
            </Grid>
        ) : (
            <Grid className={classes.root} style={pageStyle}>
                <Grid className={classes.pageImg} container direction="row" justify="center" alignItems="center"
                    style={Object.assign(pageImgStyle, {border: "1pt solid black"})} 
                >
                    <ButtonBase onClick={this.loadImageData}>
                        <RefreshIcon style={{width: "100px" ,height: "100px", color: "white"}}/>          
                    </ButtonBase>
                </Grid>
            </Grid>
        );
    }
}

SignView.propTypes = {
    classes: PropTypes.object.isRequired,
    pageIndex: PropTypes.number.isRequired,
    signatureBoxs: PropTypes.array.isRequired
};
export default connect(mapStateToProps)(withStyles(styles)(SignView));
