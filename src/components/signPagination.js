import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, ButtonBase } from "@material-ui/core";
import DropDown from "./dropDown";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import i18n from '../i18n';
import styles from "../assets/components/style/signPagination";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';

class SignPagination extends Component {

    constructor(props) {
        super(props);
        const { step, pageCount } = this.props;
        this.state = {
            pageIndex: step || 0,
            pageCount: pageCount  || 0
        };
    }
  
    componentDidUpdate(prevProps) {
      if(prevProps.pageIndex !==  this.props.pageIndex || prevProps.pageCount !==  this.props.pageCount) {
          this.setState({
            pageIndex: this.props.pageIndex,
            pageCount: this.props.pageCount
          })
      }
    }

    handeStepChange = (pageIndex) => {
        if (pageIndex >= 0 && pageIndex < this.state.pageCount) {
            this.setState({
                pageIndex
            }, ()=> {
                const { setPageIndex } = this.props;
                setPageIndex(pageIndex);
            });
        }
    }

    render() {
        const { classes, context } = this.props;
        const { localization } = context;
        const { pageIndex, pageCount } = this.state;
        let list = [];
        if (pageCount > 0) {
            for (let i = 0; i < pageCount; i++) {
                list.push({
                    key: i,
                    value: i + 1
                });
            }
        } else {
            list.push({
                key: 0,
                value: ``
            });
        }
        return (
            <Grid className={classes.root}  container direction="row" justify="center" alignItems="center">
                <Grid>
                    <ButtonBase className={classes.stepBtn}   disabled={pageCount <= 1 || pageIndex <= 0} onClick={()=> {this.handeStepChange(pageIndex - 1)}}>
                        <ArrowBackIosIcon />
                    </ButtonBase>
                </Grid>
                <Grid>
                    <Grid className={classes.pageJumping}  container direction="row" justify="center" alignItems="center">
                        <Grid>{i18n(localization, {path: "signPagination.page"})}</Grid>
                        <Grid>
                            <DropDown 
                                list={list}
                                value={pageIndex}
                                showKey={"value"}
                                valueKey={"key"}
                                style={{width: "55px", height: "100%", border: "none", borderRadius: "5%", boxShadow: "none", marginLeft: "13px" }}
                                textStyle={{
                                    textAlign: "center", paddingLeft: "2px", 
                                }}
                                onChange={(event) => {this.handeStepChange(event.target.value)}}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid>
                    <ButtonBase className={classes.stepBtn}  disabled={pageCount <= 1 || pageIndex >= pageCount - 1} onClick={()=> {this.handeStepChange(pageIndex + 1)}}>
                        <ArrowForwardIosIcon />
                    </ButtonBase>
                </Grid>
            </Grid>
        );
    }
}

SignPagination.propTypes = {
    classes: PropTypes.object.isRequired,
    pageIndex: PropTypes.any.isRequired,
    pageCount: PropTypes.any.isRequired,
    setPageIndex: PropTypes.func.isRequired
};
export default connect(mapStateToProps)(withStyles(styles)(SignPagination));
