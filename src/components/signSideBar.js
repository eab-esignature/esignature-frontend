import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
    Grid,
    ButtonBase,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Drawer
} from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import * as _ from "lodash";
import i18n from '../i18n';
import styles from "../assets/components/style/signSideBar";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';

class SignSideBar extends Component {

    constructor(props) {
        super(props);
        const { selectedField } = this.props;
        this.state = {
            sortBy: "group",
            fieldGroups: null,
            selectedField: selectedField || null,
            hover: null
        };
    }

    componentDidMount() {
        this.setState({
            fieldGroups: this.getFieldGroups()
        });
    }

    componentDidUpdate(prevProps) {
        const { fields, selected } = this.props;
        const newState = {};
        if (!_.isEqual(prevProps.fields, fields)) {
            newState.fieldGroups = this.getFieldGroups();
        }
        if (this.state.selectedField !== selected) {
            newState.selectedField = selected;
        }
        if (!_.isEmpty(newState)) {
            this.setState(newState);
        }
    }

    getFieldGroups = () => {
        const { fields } = this.props;

        const fieldGroups = [];
        fields.forEach(field => {
            if (!fieldGroups[field.lookupkey]) {
                fieldGroups[field.lookupkey] = {};
                fieldGroups[field.lookupkey].items = [];
                fieldGroups[field.lookupkey].items.push(field);
            } else {
                fieldGroups[field.lookupkey].items.push(field);
            }
        });
        return fieldGroups;
    }



    handleSelected = (seq, lookupkey, pageIndex, openCanvas = false) => {
        const { selected, onSelected } = this.props;
        if (seq && lookupkey) {
            if (
                !selected || !selected.seq || selected.lookupkey ||
                !_.isEqual(seq, selected.seq) || !_.isEqual(lookupkey, selected.lookupkey)
            ) {
                onSelected({
                    seq: seq,
                    lookupkey: lookupkey
                }, pageIndex, openCanvas);
            }
        }
    }

    getViewByFieldGroups = () => {
        const { classes, context } = this.props;
        const { localization, config } = context;
        const {
            sidebarBGColor,
            sideBarBorderSize,
            sideBarSignBtnBGColor,
            sideBarCompleteIconColor,
            sideBarSelectedBgColor,
        } = config;
        const { fieldGroups, selectedField, hover } = this.state;
        const viewByFieldGroups = [];
        for (var lookupkey in fieldGroups) {
            viewByFieldGroups.push({
                open: false,
                lookupkey: lookupkey,
                signerName: fieldGroups[lookupkey].signerName,
                fields: fieldGroups[lookupkey].items
            });
        }
        let fieldGroupsSeq = {};
        let updateFieldGroupsSeq = (lookupkey, pageIndex, seq) => {
            if (fieldGroupsSeq[lookupkey]) {
                if (fieldGroupsSeq[lookupkey][pageIndex]) {
                    fieldGroupsSeq[lookupkey][pageIndex].push(seq);
                    return fieldGroupsSeq[lookupkey][pageIndex].length;
                } else {
                    fieldGroupsSeq[lookupkey][pageIndex] = [seq];
                }
            } else {
                fieldGroupsSeq[lookupkey] = {};
                fieldGroupsSeq[lookupkey][pageIndex] = [seq];
            }
            return null;
        };
        return viewByFieldGroups.map((item, inx) => {
            return (
                <ExpansionPanel key={inx}
                    className={classes.expPanelGroup}
                    style={{
                        backgroundColor: sidebarBGColor,
                        borderBottom: `${sideBarSelectedBgColor} ${sideBarBorderSize} solid`
                    }}
                >
                    <ExpansionPanelSummary
                        style={{ padding: "0px 20px" }}
                        expandIcon={<ExpandMoreIcon />}
                    >
                        {item.signerName ? item.signerName : item.lookupkey}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails style={{ margin: "0px", padding: "0px" }}>
                        <Grid style={{ width: "100%" }}>
                            {item.fields.map((field, inx) => {
                                let lookupkeySeq = updateFieldGroupsSeq(item.lookupkey, field.pageIndex, field.seq);
                                if (lookupkeySeq) {
                                    lookupkeySeq = ` - ${lookupkeySeq}`;
                                } else {
                                    lookupkeySeq = "";
                                }
                                let isSelected = selectedField && (field.seq === selectedField.seq && item.lookupkey === selectedField.lookupkey);                                
                                let expPanelItemStyle = {
                                    width: "100%",
                                    padding: "14px 20px",
                                    cursor: "default",
                                    userSelect: "none",
                                    "&:hover": {
                                        
                                    }
                                };
                                if (isSelected) {
                                    expPanelItemStyle = Object.assign({}, expPanelItemStyle, {
                                        backgroundColor: "rgba(219, 219, 219, 1)",
                                        borderTop: "rgba(200, 200, 200, 1) 2px solid",
                                        borderBottom: "rgba(200, 200, 200, 1) 2px solid"
                                    });
                                } else if (hover && field.seq === hover.seq && item.lookupkey === hover.lookupkey) {
                                    expPanelItemStyle = Object.assign({}, expPanelItemStyle, {
                                        backgroundColor: "rgba(219, 219, 219, 1)"
                                    });
                                }
                                return (
                                    <Grid key={inx}
                                        onMouseEnter={() => {
                                            this.setState({
                                                hover: {
                                                    seq: field.seq,
                                                    lookupkey: item.lookupkey,
                                                    pageIndex: field.pageIndex
                                                }
                                            })
                                        }}
                                        onMouseOut={() => {this.setState({ hover: null })}}
                                        container direction="row" justify="space-between" alignItems="center"
                                        className={isSelected ? classes.expPaneSelectedlItem : classes.expPanelItem}
                                        style={expPanelItemStyle}
                                    >
                                        <Grid className={classes.expPanelItemLabel} onClick={() => {
                                            this.handleSelected(field.seq, item.lookupkey, field.pageIndex);
                                        }}>
                                            {`Page ${Number(field.pageIndex) + 1}${lookupkeySeq}`}
                                        </Grid>
                                        {
                                            field.imgKey ? <CheckCircleIcon style={{ color: sideBarCompleteIconColor, marginRight: "20px" }} /> :
                                            <ButtonBase
                                                className={classes.expPanelItemSignBtn}
                                                style={{ backgroundColor: sideBarSignBtnBGColor }}
                                                onClick={() => {
                                                    this.handleSelected(field.seq, item.lookupkey, field.pageIndex, true);
                                                }}
                                            >
                                                {i18n(localization, { path: "signSideBar.signBtn" })}
                                            </ButtonBase>
                                        }
                                    </Grid>
                                )
                            })}
                        </Grid>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            );
        });
    }

    render() {
        const { classes, context, open, onSave } = this.props;
        const { localization, config } = context;
        const {
            sideBarSelectedBgColor,
            sideBarBorderSize,
            sidebarBGColor,
            sideBarTitle,
            sideBarTitleColor,
            sideBarTitleFontSize,
            applyBtnColor,
            applyBtnBGColor,
            applyBtnDisabledColor,
            applyBtnDisabledBGColor,
        } = config;

        let saveSignatureStyle = {
            margin: "20px",
            width: "calc(100% - 40px)",
            padding: "10px",
            color: applyBtnColor,
            backgroundColor: applyBtnBGColor,
            fontSize: "17px",
            fontWeight: "bold"
        };
        if (!onSave) {
            saveSignatureStyle = Object.assign({}, saveSignatureStyle, {
                backgroundColor: applyBtnDisabledColor,
                color: applyBtnDisabledBGColor
            })
        }
        return (
            <Drawer
                className={classes.root}
                variant="persistent"
                anchor="right"
                open={open}
                classes={{
                    paper: classes.paper,
                }}
            >
                <Grid style={{
                    backgroundColor: sidebarBGColor,
                    width: "100%",
                    height: "100%"
                }}>
                    {
                        !sideBarTitle ? null :
                            <Grid
                                className={classes.expPanelTitle}
                                style={{
                                    fontSize: sideBarTitleFontSize,
                                    color: sideBarTitleColor,
                                    borderBottom: `${sideBarSelectedBgColor} ${sideBarBorderSize} solid`
                                }}
                            >
                                <Grid>{i18n(localization, { path: "signSideBar.title" })}</Grid>
                            </Grid>
                    }

                    <Grid style={{ overflow: "auto", maxHeight: "calc(100% - 200px)" }}>
                        {this.getViewByFieldGroups()}
                    </Grid>

                    <ButtonBase onClick={onSave} disabled={!onSave} className={classes.expPanelSaveBtn} style={saveSignatureStyle}>
                        {i18n(localization, { path: "signSideBar.saveSignature" })}
                    </ButtonBase>
                </Grid>
            </Drawer>
        );
    }
}

SignSideBar.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default connect(mapStateToProps)(withStyles(styles)(SignSideBar));
