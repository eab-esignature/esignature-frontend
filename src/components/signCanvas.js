import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, ButtonBase, Dialog } from "@material-ui/core";
import SignaturePad from "react-signature-canvas";
import * as _ from "lodash";
import i18n from '../i18n';
import util from "../tool/util";
import styles from "../assets/components/style/signCanvas";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';

class SignCanvas extends Component {

    constructor(props) {
        super(props);
        this.state = {
            drawDataURL: null,
            drawUndoList: [],
            undoFlag: false,
            usePrevious: false,
            canvasRate: 1
        };
    }

    componentDidUpdate(prevProps) {
        const { open, history, canvasWidth, canvasHeight } = this.props;
        if (!_.isEqual(open, this.state.open)) {
            this.setState({
                open: open
            }, () => {
                if (!_.isEmpty(this.sigPad)) {
                    if (open) {
                        if (history) {
                            this.sigPad.fromDataURL(history);
                        }
                        this.setState({
                            drawDataURL: history
                        });
                    } else {
                        this.sigPad.clear();
                    }
                }
            });
        }
        if (!_.isEqual(canvasWidth, prevProps.canvasWidth) || !_.isEqual(canvasHeight, prevProps.canvasHeight)) {
            this.getCanvasProps();
        }
    }
    componentDidMount() {
        this.getCanvasProps();
    }

    sigPad = {};

    getCanvasProps = () => {
        const { classes, canvasWidth, canvasHeight, context } = this.props;
        const {
            canvasMaxWidth,
            canvasMaxHeight,
            canvasLengthUnit,
            // canvasZoomRate
        } = context.config;
        let canvasRate = 1;
        let canvasProps = {
            className: classes.signCanverStyle,
            width: 0,
            height: 0
        }
        if (canvasMaxWidth < canvasMaxHeight) {
            let magnification = util.calcMagnification(
                canvasWidth,
                canvasMaxWidth,
                canvasHeight,
                canvasMaxHeight
            );
            canvasProps.width = `${magnification.small}${canvasLengthUnit}`;
            canvasProps.height = `${magnification.big}${canvasLengthUnit}`;
            canvasRate = canvasWidth / 0.75 * 1.5 / magnification.small;
        } else {
            let magnification = util.calcMagnification(
                canvasHeight,
                canvasMaxHeight,
                canvasWidth,
                canvasMaxWidth
            );
            canvasProps.height = `${magnification.small}${canvasLengthUnit}`;
            canvasProps.width = `${magnification.big}${canvasLengthUnit}`;
            canvasRate = canvasWidth / 0.75 * 1.5 / magnification.big;
        }
        this.setState({
            canvasRate: canvasRate,
            canvasProps: canvasProps
        });
    }

    handleChange = () => {
        this.setState({
            usePrevious: false,
            drawDataURL: this.sigPad.getSignaturePad().toDataURL("image/png"),
            undoFlag: true
        });
    }

    handleUndo = () => {
        var data = this.sigPad.getSignaturePad().toData();
        if (data) {
            data.pop(); // remove the last dot or line
            this.sigPad.getSignaturePad().fromData(data);
            this.setState({
                usePrevious: false,
                drawDataURL: this.sigPad.getSignaturePad().toDataURL("image/png")
            })
        }
    }

    handleClear = () => {
        this.sigPad.clear();
        this.setState({
            usePrevious: false,
            drawDataURL: null,
            drawUndoList: []
        })
    }

    handlePrevious = () => {
        const { previous } = this.props;
        if (previous) {
            this.sigPad.clear();
            this.sigPad.fromDataURL(previous);
            this.setState({
                usePrevious: true,
                drawDataURL: previous
            });
        }
    }

    handleApply = () => {
        const { onSave, previousKey } = this.props;
        const { drawDataURL, usePrevious, canvasRate } = this.state;
        if (usePrevious) {
            onSave(previousKey, true);
        } else {
            onSave(drawDataURL, false, canvasRate);
        }
        this.handleClose();
    }

    handleClose = () => {
        this.handleClear();
        this.props.onClose();
    }

    render() {
        const { classes, open, previous, context } = this.props;
        const { localization, config } = context;
        const {
            canvasFixedSize,
            canvasMaxWidth,
            canvasMaxHeight,
            canvasLengthUnit,
            canvasActionPanel,
            canvasPrevious,
            canvasUndo,
            canvasClear,
            canvasPreview
        } = config;
        const { drawDataURL, undoFlag, canvasProps } = this.state;

        let mainContainerSize = {};
        if (canvasFixedSize) {
            mainContainerSize.width = `${canvasMaxWidth}${canvasLengthUnit}`;
            mainContainerSize.height = `${canvasMaxHeight}${canvasLengthUnit}`;
        }
        return (
            open ?
                <Dialog open={open} maxWidth={false}>
                    <Grid className={classes.root}>
                        <Grid className={classes.dialogTitle}>
                            {i18n(localization, { path: "signCanvas.title" })}
                        </Grid>
                        <Grid className={classes.signPanel}>
                            <Grid className={classes.mainContainer} style={mainContainerSize} container direction="row" justify="center" alignItems="center">
                                <SignaturePad
                                    canvasProps={canvasProps}
                                    ref={(ref) => { this.sigPad = ref }}
                                    penColor="black" // color default black
                                    minWidth={0.5}
                                    maxWidth={2.5} // size default max 2.5 and min 0.5
                                    onBegin={() => {}}
                                    onEnd={this.handleChange}
                                />
                            </Grid>
                            {
                                !canvasActionPanel ? null :
                                <Grid className={classes.actionPanel} container direction="row" justify="space-between" alignItems="center">
                                    {
                                        !canvasPrevious ? null :
                                        <ButtonBase
                                            className={classes.actionPanelBtn}
                                            onClick={this.handlePrevious}
                                            disabled={!previous}
                                        >
                                            {i18n(localization, { path: "signCanvas.previousSignature" })}
                                        </ButtonBase>
                                    }
                                    <Grid>
                                        <Grid container direction="row" justify="flex-end" alignItems="center">
                                            {
                                                !canvasUndo ? null :
                                                <ButtonBase
                                                    className={classes.actionPanelBtn}
                                                    onClick={this.handleUndo}
                                                    disabled={!undoFlag}
                                                >
                                                    {i18n(localization, { path: "signCanvas.undo" })}
                                                </ButtonBase>
                                            }
                                            {
                                                !canvasClear ? null :
                                                <ButtonBase
                                                    className={classes.actionPanelBtn}
                                                    onClick={this.handleClear}
                                                    disabled={!drawDataURL}
                                                >
                                                    {i18n(localization, { path: "signCanvas.clear" })}
                                                </ButtonBase>
                                            }
                                        </Grid>
                                    </Grid>
                                </Grid>
                            }

                        </Grid>
                        <Grid className={classes.dialogAction} container direction="row" justify="flex-end" alignItems="center">
                            <ButtonBase className={classes.dialogActionBtn} onClick={this.handleClose}>
                                {i18n(localization, { path: "signCanvas.cannel" })}
                            </ButtonBase>
                            <ButtonBase className={classes.dialogActionApply} onClick={this.handleApply} disabled={!drawDataURL}>
                                {i18n(localization, { path: "signCanvas.apply" })}
                            </ButtonBase>
                        </Grid>
                        {drawDataURL && canvasPreview
                            ? <img className={styles.sigImage} src={drawDataURL} alt={""} />
                            : null}
                    </Grid>
                </Dialog> : null
        );
    }
}

SignCanvas.propTypes = {
    classes: PropTypes.object.isRequired
};
export default connect(mapStateToProps)(withStyles(styles)(SignCanvas));
