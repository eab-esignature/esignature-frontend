import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Typography, InputBase, Select, MenuItem } from "@material-ui/core";
import styles from "../assets/components/style/dropDown";
//redux
import { connect } from 'react-redux';
import { mapStateToProps } from '../reducers/mapState';

class DropDown extends React.Component {
  constructor(props) {
    super(props);
    const { value, list } = this.props;
    this.state = {
      value: value || "",
      list: list || []
    };
    this.handleChange = this.handleChange.bind(this);
  }
  
  componentDidUpdate(prevProps) {
    if(prevProps.value !==  this.props.value || prevProps.list !==  this.props.list) {
        this.setState({
          value: this.props.value,
          list: this.props.list
        })
    }
  }

  handleChange = event => {
    const { onChange } = this.props;
    this.setState({
      value: event.target.value
    });
    onChange(event);
  };

  render() {
    const { classes, style, textStyle, showKey, valueKey } = this.props;
    const { value, list } = this.state;
    return (
      <Select
        className={classes.root}
        style={style || {}}
        value={value}
        onChange={this.handleChange}
        input={<InputBase />}
      >
        {list.map((item, inx) => (
          <MenuItem key={inx} value={valueKey ? item[valueKey] : item}>
            <Typography className={classes.text} style={textStyle || {}}>
              {showKey ? item[showKey] : item}
            </Typography>
          </MenuItem>
        ))}
      </Select>
    );
  }
}
DropDown.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  textStyle: PropTypes.object,
  onChange: PropTypes.func,
  list: PropTypes.array,
  showKey: PropTypes.string,
  valueKey: PropTypes.string,
  value: PropTypes.any
};
export default connect(mapStateToProps)(withStyles(styles)(DropDown));
