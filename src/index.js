import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Theme Styles
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";

// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from "redux-thunk";

// Reducer
import rootReducer from "./reducers";
const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <App />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
);
