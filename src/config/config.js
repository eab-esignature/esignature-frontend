const getConfig = () => {
    const config = {};
    config.localization = "EN";
    //[signView]
    config.pageWidth = "595pt";
    config.pageHeight = "842pt";
    config.pageSpacing = "13px";
    config.pageBoxBGColor = "rgba(81, 81, 81, 1)";
    config.showSignBox = true;
    config.signBoxColor = "rgba(190, 185, 171, 1)";
    config.signBoxFontSize = "10pt";
    config.signBoxBGColor = "rgba(254, 250, 202, 1)";
    config.signBoxHoverBGColor = "rgba(250, 245, 202, 1)";
    config.signImgClear = true;
    config.onlySelectedSignImgClear = false;
    config.signImgBGColor = "rgba(0,0,0,0~1)";
    config.signImgBorderSize = "2pt";
    config.signImgHoverBorderColor = "rgba(226, 239, 246, 1)";
    config.signImgSelectedBorderColor = "rgba(169, 0, 47, 1)";
    config.signViewMaxZoom = 3;
    //[signView]
    config.signLabelColor = "rgba(128, 128, 128, 1)";
    config.signLabelFontSize = "7pt";
    config.showSignDate = true;
    config.signDateFormat = "DD/MM/YYYY hh:mm:ss";
    config.signDateMoveX = 0;
    config.signDateMoveY = -20;
    config.showGPS = true;
    config.gpsDesimalPoint = 5;
    config.gpsFailPath = "gpsFailPath";
    config.gpsFormat = "(x:{x}, y:{y})";
    //[canvas]
    config.canvasZoomRate = 1.5;
    config.canvasFixedSize = true;
    config.canvasMaxWidth = 600;
    config.canvasMaxHeight = 400;
    config.canvasLengthUnit = "px";
    config.canvasActionPanel = true;
    config.canvasPrevious = true;
    config.canvasClear = true;
    config.canvasUndo = false;
    config.canvasPreview = false;
    //[sideBar]
    config.sideBarDefaultOpen = false;
    config.sideBarColor = null;
    config.sideBarFontSize = "14px";
    config.sideBarBorderSize = "2px";
    config.sidebarBGColor = "rgba(237, 237, 237, 1)";
    config.sideBarTitle = true;
    config.sideBarTitleColor = null;
    config.sideBarTitleFontSize = "19px";
    config.sideBarSelectedBgColor = "rgba(219, 219, 219, 1)";
    config.sideBarSelectedBorderColor = "rgba(200, 200, 200, 1) ";
    config.sideBarSignBtnBGColor = "rgba(209, 231, 224, 1)";
    config.sideBarCompleteIconColor = "rgba(0, 182, 100, 1)";
    config.applyBtnColor = "rgba(237, 237, 237, 1)";
    config.applyBtnBGColor = "rgba(169, 0, 47, 1)";
    config.applyBtnDisabledColor = "rgba(176, 176, 176, 1)";
    config.applyBtnDisabledBGColor = "rgba(224, 225, 226, 1)";
    config.topBarBGColor = "rgba(51, 51, 51, 1)";
    //[AutoSave]
    config.autoSave = true;
    config.autoSaveMsg = "Do you want to save it?";
    config.autoSaveMsgEN = "Do you want to save it?";
    config.autoSaveMsgCN = "你希望保存吗?";
    return config;
}

export default getConfig();