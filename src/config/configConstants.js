const getConfigConstants = () => {
    const config = {};
    config.localization = "EN";
    
    config.pageWidth = "595pt";
    config.pageHeight = "842pt";
    config.pageSpacing = "13px";
    config.pageBoxBGColor = "rgba(81, 81, 81, 1)";
    
    config.SIGN_VIEW = {};
    config.SIGN_VIEW.bgColor = "rgba(81, 81, 81, 1)";
    config.SIGN_VIEW.pageWidth = "595pt";
    config.SIGN_VIEW.pageHeight = "842pt";
    config.SIGN_VIEW.pageSpacing = "13px";
    config.SIGN_VIEW.signBoxSDisplay = true;
    config.SIGN_VIEW.signBoxFontSize = "10pt";
    config.SIGN_VIEW.signBoxColor = "rgba(190, 185, 171, 1)";
    config.SIGN_VIEW.signBoxBGColor = "rgba(254, 250, 202, 1)";
    config.SIGN_VIEW.signCurBoxBGColor = "rgba(250, 245, 202, 1)";
    config.SIGN_VIEW.signImgBGColor = "rgba(0,0,0,0~1)";
    config.SIGN_VIEW.signCurBorColor = "rgba(226, 239, 246, 1)";
    config.SIGN_VIEW.signCurBorderSize = "2pt";
    config.SIGN_VIEW.signSelectedBorColor = "rgba(169, 0, 47, 1)";
    config.SIGN_VIEW.signImgClear = true;
    config.SIGN_VIEW.signImgClearWidth = "30px";
    config.SIGN_VIEW.signImgClearHeight = "30px";
    config.SIGN_VIEW.signImgClearMarginRight = "10px";
    config.SIGN_VIEW.signImgSignleClear = false;
    config.SIGN_VIEW.showSignDate = true;
    config.SIGN_VIEW.signFontSize = "7pt";
    config.SIGN_VIEW.signDateColor = "rgba(128, 128, 128, 1)";
    config.SIGN_VIEW.signDateMoveX = 0;
    config.SIGN_VIEW.signDateMoveY = -20;
    config.SIGN_VIEW.signDateFormat = "DD/MM/YYYY hh:mm:ss";

    config.canvasZoomRate = 1.5;
    config.canvasFixedSize = true;
    config.canvasMaxWidth = 600;
    config.canvasMaxHeight = 400;
    config.canvasLengthUnit = "px";
    config.canvasActionPanel = true;
    config.canvasPrevious = true;
    config.canvasClear = true;
    config.canvasUndo = false;
    config.canvasPreview = false;

    config.sideBarDefaultOpen = false;
    config.sideBarColor = null;
    config.sideBarFontSize = "14px";
    config.sideBarBorderSize = "2px";
    config.sidebarBGColor = "rgba(237, 237, 237, 1)";
    config.sideBarTitle = true;
    config.sideBarTitleColor = null;
    config.sideBarTitleFontSize = "19px";
    config.sideBarSelectedBgColor = "rgba(219, 219, 219, 1)";
    config.sideBarSelectedBorderColor = "rgba(200, 200, 200, 1) ";
    config.sideBarSignBtnBGColor = "rgba(209, 231, 224, 1)";
    config.sideBarCompleteIconColor = "rgba(0, 182, 100, 1)";
    config.applyBtnColor = "rgba(237, 237, 237, 1)";
    config.applyBtnBGColor = "rgba(169, 0, 47, 1)";
    config.applyBtnDisabledColor = "rgba(176, 176, 176, 1)";
    config.applyBtnDisabledBGColor = "rgba(224, 225, 226, 1)";

    config.topBarBGColor = "rgba(51, 51, 51, 1)";
    config.SIGN_TOOL = {};
    config.SIGN_TOOL.bgColor = "rgba(51, 51, 51, 1)";

    return config;
}

export default getConfigConstants();