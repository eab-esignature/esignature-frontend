import i18n from "./index";

describe('i18n index', () => {
  it('[function]i18n [cause]success', () => {
    expect(i18n({
      path: "navBar.Save",
    })).toBe('Save');
  });
});