const getTextStore = () => {
    const textStore = {};

    textStore.signPagination = {};
    textStore.signPagination.page = "Page";

    textStore.signView = {};
    textStore.signView.signatureBox = {};
    textStore.signView.signatureBox.clickTip = "Click here to sign";

    textStore.signCanvas = {};
    textStore.signCanvas.title = "Sign here";
    textStore.signCanvas.previousSignature = "Previous Signature";
    textStore.signCanvas.clear = "Clear";
    textStore.signCanvas.undo = "Undo";
    textStore.signCanvas.cannel = "Cannel";
    textStore.signCanvas.apply = "Apply";

    textStore.signSideBar = {};
    textStore.signSideBar.title = "Signature Field";
    textStore.signSideBar.signBtn = "Sign";
    textStore.signSideBar.saveSignature = "Save Signature";

    textStore.msgDialog = {};
    textStore.msgDialog.saveTitle = "Save Signature";
    textStore.msgDialog.saveMsg = "Do you want to save it?";
    textStore.msgDialog.cannel = "Cannel";
    textStore.msgDialog.save = "Save";
    return textStore;
}

export default getTextStore();
