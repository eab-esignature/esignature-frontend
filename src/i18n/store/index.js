import EN from './EN';
import zht from './zht';
import CN from './CN';

export default {
    EN,
    zht,
    CN
};
