const getTextStore = () => {
    const textStore = {};
    textStore.language = "en";

    textStore.signView = {}
    textStore.signView.signatureBox = {};
    textStore.signView.signatureBox.text = "Click here to sign";
    return textStore;
}

export default getTextStore();
