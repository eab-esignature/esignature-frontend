import store from './store';
import config from '../config/config';

/**
 * @description if can not found the text, throw error
 * @param {string} path - the path of the text
 * */
const pathNotFoundError = path => {
  throw new Error(`text not found. path: "${path}"`);
};
/**
 * @description get the text form the textStore
 * @param {string} path - the path of the text
 * @param {object} replace - replace target words in text
 * @example
 * i18n({
 *   path: "user.addressButton",
 *   replace: {
 *     name: "John",
 *   },
 * })
 * @return {string|object}
 * */
const i18n = (language, { path, replace = {} }) => {
  const pathArray = path.split(".");
  const defaultStore = store[config.localization];
  let textData = store[config.localization];
  if (language) {
    if (store[language]) {
      textData = store[language];
    } 
  }
  // get the data
  try {
    pathArray.forEach(pathData => {
      if (textData[pathData] === undefined) {
        throw new Error("textStoreEnd");
      }
      textData = textData[pathData];
    });
  } catch (e) {
    if (e.message !== "textStoreEnd") {
      pathNotFoundError(path);
    }
    textData = defaultStore;
    pathArray.forEach(pathData => {
      if (textData[pathData] === undefined) {
        pathNotFoundError(path);
      }
      textData = textData[pathData];
    });
  }
  // check the return type
  if (typeof textData !== "string" && typeof textData !== "number") {
    pathNotFoundError(path);
  }

  // replace words
  const replaceList = Object.keys(replace);
  if (replaceList.length !== 0) {
    replaceList.forEach(replaceKey => {
      textData = textData.replace(`{${replaceKey}}`, replace[replaceKey]);
    });
  }

  return textData;
};
export default i18n;
