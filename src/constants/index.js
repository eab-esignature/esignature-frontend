// Export as DEFAULT
import ACTION_TYPES from "./ACTION_TYPES";

// Export one-by-one by CONST
import * as REDUCER_TYPES from "./REDUCER_TYPES";
import { PATHS as WEB_PATHS } from "./WEB_PATHS";

export { ACTION_TYPES, REDUCER_TYPES, WEB_PATHS };
