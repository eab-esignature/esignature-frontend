import React, { Component } from "react";
import { Router } from "@reach/router";
import './reset.css';
// Routes
import Error from "./containers/error";
import Main from "./containers/main";
import Signature from "./containers/signature";

class App extends Component {
  render() {
    return (
        <Router>
          <Main path={`/`}>
            <Signature path={`/`} />
            <Error default />
          </Main>
        </Router>
    );
  }
}
export default App;