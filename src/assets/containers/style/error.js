const styles = theme => ({
    container: {
        fontSize: "calc(10px + 2vmin)",
        textAlign: "center"
    },
    black: {
        color: "black"
    }    
});
export default styles;
  