import config from '../../../config/config';

const pageSpacing = config.pageSpacing;
const styles = theme => ({
    root: {
        position: "fixed",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: "100%",
        height: "100%"
    },
    mainView: {
        width: "100%",
        height: "100%"
    },
    topBar: {
        backgroundColor: "rgba(51, 51, 51, 1)",
        width: "100%",
        height: "50px",
        boxShadow: `0px 3px 6px rgba(204, 210, 213, 1)`
    },
    topBarItem: {
        height: "50px"
    },
    topToolMenu: {
        height: "50px",
        color: "white",
        borderRight: "2px solid rgba(81, 81, 81, 1)",
        marginLeft: "5px"
    },
    topToolBtn: {
        height: "50px",
        width: "60px",
        borderLeft: "2px solid rgba(81, 81, 81, 1)",
        color: "rgba(176, 176, 176, 1)",
        "&:disabled": {
            color: "rgba(81, 81, 81, 1)",
        }
    },
    container: {
        backgroundColor: "rgba(85, 84, 84, 1)",
        height: "calc(100% - 50px)",
        width: "100%",
        overflow: "auto",
        cursor: "default",
        userSelect: "none",
        paddingBottom: pageSpacing
    },
    pdfPageBox: {
        backgroundColor: "white",
        height: "297mm",
        minHeight: "297mm",
        width: "210mm",
        minWidth: "210mm",
        border: "red solid 1px",
        margin: "auto"
    }
});
export default styles;
  