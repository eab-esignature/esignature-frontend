const styles = theme => ({
    root: {
        marginTop: "13px",
        position: "relative",
        transformOrigin: "top left",
    },
    pageImg: {
        width: "595pt",
        height: "842pt"
    },
    signatureBox: {
        color: "rgba(190, 185, 171, 1)",
        fontSize: "10pt",
        backgroundColor: "rgba(254, 250, 202, 1)",
    },
    signatureImg: {
        backgroundColor: "rgba(0,0,0,0~1)",
        backgroundSize: "100% 100%",
    },
    clearBtn: {
        height: "30px",
        width: "30px",
        marginLeft: `calc(0px - 30px - 10px)`,
        borderRadius: "50%"
    }
});
export default styles;
  