const styles = theme => ({
    root: {
        height: "100%",
        width: "100%",
        fontSize: "14px",
        fontWeight: "bold",
        overflow: "auto"
    },
    paper: {
        backgroundColor: "rgba(237, 237, 237, 1)",
        width: "300px",
    },
    expPanelTitle: {
        fontSize: "19px",
        borderBottom: "rgba(219, 219, 219, 1) 2px solid",
        padding: "30px 20px"
    },
    expPanelGroup: {
        width: "100%",
        margin: "0px",
        border: "none",
        padding: "0px 0px",
        backgroundColor: "rgba(237, 237, 237, 1)",
        borderBottom: "rgba(219, 219, 219, 1) 2px solid",
        boxShadow: "none"
    },
    expPanelItem: {
        width: "100%",
        padding: "14px 20px",
        cursor: "default",
        userSelect: "none",
        "&:hover": {
            backgroundColor: "rgba(219, 219, 219, 1)",
        }
    },
    expPaneSelectedlItem: {
        width: "100%",
        padding: "14px 20px",
        cursor: "default",
        userSelect: "none",
        backgroundColor: "rgba(219, 219, 219, 1)",
        borderTop: "rgba(200, 200, 200, 1) 2px solid",
        borderBottom: "rgba(200, 200, 200, 1) 2px solid"
    },
    expPanelItemLabel: {
        width: "calc(100% - 90px)",
    },
    expPanelItemSignBtn: {
        width: "90px",
        height: "35px",
        fontWeight: "bold",
        backgroundColor: "rgba(209, 231, 224, 1)"
    },
    expPanelSaveBtn: {
        margin: "20px",
        width: "calc(100% - 40px)",
        padding: "10px",
        color: "rgba(237, 237, 237, 1)",
        backgroundColor: "rgba(169, 0, 47, 1)",
        fontSize: "17px",
        fontWeight: "bold",
        "&:disabled": {
            backgroundColor: "rgba(224, 225, 226, 1)",
            color: "rgba(176, 176, 176, 1)",
            fontWeight: "bold",
        }
    },
    disabled: {
        backgroundColor: "rgba(224, 225, 226, 1)",
        color: "rgba(176, 176, 176, 1)",
        fontWeight: "bold",
    }
});
export default styles;
  