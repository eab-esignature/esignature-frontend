const styles = theme => ({
    root: {
        padding: "20px"
    },
    msgSpacing: {
        height: "20px"
    },
    msgTitle: {
        fontSize: "16px",
        fontWeight: "bold",
        cursor: "default",
        userSelect: "none",
    },
    msgContent: {
        width: "600px"
    },
    msgButton: {
        border: "solid 1px black",
        borderRadius: "5%",
        padding: "8px 14px",
        margin: "0px 7px",
        fontWeight: "bold"
    },
    msgButtonDisable: {
        border: "solid 1px rgba(150, 150, 150, 1)",
        backgroundColor: "rgba(150, 150, 150, 1)",
    },
    dialogActionBtn: {
        border: "solid 1px black",
        borderRadius: "5%",
        padding: "8px 14px",
        margin: "0px 7px",
        fontWeight: "bold"
    },
    dialogActionApply: {
        border: "solid 1px rgba(169, 0, 47, 1)",
        backgroundColor: "rgba(169, 0, 47, 1)",
        color: "white",
        borderRadius: "5%",
        padding: "8px 14px",
        margin: "0px 7px",
        fontWeight: "bold",
        "&:disabled": {
            border: "solid 1px rgba(150, 150, 150, 1)",
            backgroundColor: "rgba(150, 150, 150, 1)",
        }
    }
});
export default styles;
  