const styles = theme => ({
  root: {
    backgroundColor: "white",
    border: `2px solid rgba(49, 120, 116, 1)`,
    borderRadius: "3%",
    boxShadow: `0px 3px 6px rgba(204, 210, 213, 1)`,
    width: "216px",
    height: "42px"
  },
  text: {
    fontSize: "16px",
    textAlign: "left",
    paddingLeft: "19px"
  }
});
export default styles;
