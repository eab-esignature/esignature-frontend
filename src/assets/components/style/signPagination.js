const styles = theme => ({
    root: {
        height: "50px",
        color: "white",
        borderRight: "2px solid rgba(81, 81, 81, 1)",
        marginLeft: "5px"
    },
    pageJumping: {
        height: "50px",
        padding: "0px 23px",
        borderLeft: "2px solid rgba(81, 81, 81, 1)",
        borderRight: "2px solid rgba(81, 81, 81, 1)",
    },
    stepBtn: {
        color: "rgba(176, 176, 176, 1)",
        width: "60px",
        "&:disabled": {
            color: "rgba(81, 81, 81, 1)",
        }
    }
});
export default styles;
  