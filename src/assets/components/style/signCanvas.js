const styles = theme => ({
    root: {
        backgroundColor: "white",
        padding: "20px",
        position: "relative"
    },
    dialogTitle: {
        fontSize: "16px",
        textAlign: "center",
        fontWeight: "bold",
        cursor: "default",
        userSelect: "none"
    },
    signPanel: {
        border: "solid 1px rgba(79, 168, 229, 1)",
        margin: "20px 0px",
    },
    mainContainer: {
        backgroundColor: "rgba(150, 150, 150, 1)"
    },
    signCanverStyle: {
        backgroundColor: "white"
    },
    actionPanel: {
        borderTop: "solid 1px rgba(79, 168, 229, 1)",
        padding: "10px"
    },
    actionPanelBtn: {
        color: "rgba(35, 119, 183, 1)",
        fontWeight: "bold",
        margin: "0px 10px",
        "&:disabled": {
            color: "rgba(150, 150, 150, 1)",
            fontWeight: "bold",
        }
    },
    actionPanelClear: {
        color: "rgba(35, 119, 183, 1)",
        fontWeight: "bold"
    },
    dialogActionBtn: {
        border: "solid 1px black",
        borderRadius: "5%",
        padding: "8px 14px",
        margin: "0px 7px",
        fontWeight: "bold"
    },
    dialogActionApply: {
        border: "solid 1px rgba(169, 0, 47, 1)",
        backgroundColor: "rgba(169, 0, 47, 1)",
        color: "white",
        borderRadius: "5%",
        padding: "8px 14px",
        margin: "0px 7px",
        fontWeight: "bold",
        "&:disabled": {
            border: "solid 1px rgba(150, 150, 150, 1)",
            backgroundColor: "rgba(150, 150, 150, 1)",
        }
    }
});
export default styles;
  